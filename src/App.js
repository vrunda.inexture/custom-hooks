import React from 'react';
import Task1 from './components/Task1';
import Task2 from './components/Task2';
import Task3 from './components/Task3';
import Task4 from './components/Task4';
import Task5 from './components/Task5';
import Task7 from './components/Task7';
import Task6 from './components/Task6';
import './App.css';

const App = () => {
  return (
    <div className='App'>
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-md-6 p-5 bg-light'>
            <Task1 />
          </div>
          <div className='col-md-6 p-5 bg-light'>
            <Task2 />
          </div>
          <div className='col-md-6 p-5'>
            <Task3 />
          </div>
          <div className='col-md-6 p-5'>
            <Task4 />
          </div>
          <div className='col-md-6 p-5 bg-light'>
            <Task5 />
          </div>
          <div className='col-md-6 p-5 bg-light'>
            <Task6 />
          </div>
          <div className='col-md-12 p-5'>
            <Task7 />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
