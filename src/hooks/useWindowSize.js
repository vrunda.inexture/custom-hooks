import { useState, useEffect } from 'react';

export const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
    resolution: undefined,
  });

  useEffect(() => {
    function handleResize() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
        resolution: `${window.screen.width} x ${window.screen.height}`,
      });
    }
    handleResize();
    window.onresize = handleResize;
  }, []);
  return windowSize;
};
