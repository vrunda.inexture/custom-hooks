import { useState } from 'react';

const useValidation = () => {
  const [errMsg, setErrMsg] = useState('');

  const validate = (email) => {
    let isValid = email
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
    if (isValid) {
      setErrMsg('Email address is valid!');
    } else {
      setErrMsg('Please Enter correct email address!');
    }
  };
  return { validate, errMsg };
};

export default useValidation;
