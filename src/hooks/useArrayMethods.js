import { useState } from 'react';

const useArrayMethods = () => {
  const originalArr = [10, 20, 50, 85, 25, 5];
  const [resultArr, setResultArr] = useState(originalArr);

  const push = (val) => {
    if (val) {
      let tempArr = resultArr;
      tempArr.push(val);
      setResultArr([...tempArr]);
    }
  };
  const pop = () => {
    let tempArr = resultArr;
    tempArr.pop();
    setResultArr([...tempArr]);
  };
  const reverse = () => {
    let tempArr = resultArr;
    tempArr.reverse();
    setResultArr([...tempArr]);
  };
  const reset = () => {
    setResultArr([...originalArr]);
  };
  return { originalArr, resultArr, push, pop, reverse, reset };
};

export default useArrayMethods;
