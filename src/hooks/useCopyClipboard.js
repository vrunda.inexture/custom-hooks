const useCopyClipboard = () => {
  const copy = (text) => {
    let string = text || '';
    navigator.clipboard
      .writeText(string)
      .then(() => {
        console.log('Copying to clipboard was successful!');
      })
      .catch((err) => {
        console.error('Could not copy text: ', err);
      });
  };
  return { copy };
};

export default useCopyClipboard;
