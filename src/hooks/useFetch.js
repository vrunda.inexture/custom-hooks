import { useState } from 'react';

const useFetch = () => {
  const [data, setData] = useState({});
  const [errMsg, setErrMsg] = useState(null);
  const fetchData = (url, option) => {
    console.log(url, option);
    fetch(`${url}?${option}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        setData(data.results[0]);
      })
      .catch((err) => setErrMsg(err.message));
  };
  return { fetchData, data, errMsg };
};

export default useFetch;
