const useStorage = () => {
  const saveToLocal = (key, value) => {
    localStorage.setItem(key, value);
  };
  const saveToSession = (key, value) => {
    sessionStorage.setItem(key, value);
  };
  return { saveToLocal, saveToSession };
};

export default useStorage;
