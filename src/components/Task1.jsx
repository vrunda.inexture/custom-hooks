import React, { Fragment, useState } from 'react';
import useFetch from '../hooks/useFetch';

const Task1 = () => {
  const [url, setUrl] = useState('');
  const [option, setOption] = useState('');
  const { fetchData, data, errMsg } = useFetch();

  return (
    <Fragment>
      <h1>1. Fetch data</h1>
      <hr />
      <div className='input-group'>
        <input
          type='text'
          className='form-control'
          placeholder='Enter url'
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          onFocus={() => setUrl('')}
        />
        <span className='input-group-text'>?</span>
        <input
          type='text'
          className='form-control'
          placeholder='enter options'
          value={option}
          onChange={(e) => setOption(e.target.value)}
        />
      </div>
      <button
        type='button'
        className='btn btn-primary'
        onClick={() => fetchData(url, option)}
      >
        Fetch
      </button>
      {data.email && (
        <>
          <p>Phone: {data.phone}</p>
          <p>Email: {data.email} </p>
          <p>Gender: {data.gender}</p>
        </>
      )}
      {errMsg && <p className='text-danger'>{errMsg}</p>}
    </Fragment>
  );
};

export default Task1;
