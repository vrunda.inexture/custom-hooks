import React, { Fragment } from 'react';
import { useLocation } from '../hooks/useLocation';

const Task3 = () => {
  const { latitude, longitude, error } = useLocation();

  return (
    <Fragment>
      <h1>3. Location</h1>
      <hr />
      <p>Latitude: {latitude}</p>

      <p>Longitude: {longitude}</p>
      {error && <p className='text-danger'>Error: {error} </p>}
    </Fragment>
  );
};

export default Task3;
