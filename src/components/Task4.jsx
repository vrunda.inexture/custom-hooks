import React, { Fragment, useState } from 'react';
import useValidation from '../hooks/useValidation';

const Task4 = () => {
  const [email, setEmail] = useState('');

  const { validate, errMsg } = useValidation();

  return (
    <Fragment>
      <h1>4. Validation</h1>
      <hr />
      <label htmlFor='email' className='form-label'>
        Email
      </label>
      <input
        type='email'
        className='form-control'
        id='email'
        placeholder='name@example.com'
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        onFocus={() => setEmail('')}
      />
      <button
        type='button'
        className='btn btn-primary'
        onClick={() => validate(email)}
      >
        Validate
      </button>

      {errMsg && <p className='text-danger'>{errMsg}</p>}
    </Fragment>
  );
};

export default Task4;
