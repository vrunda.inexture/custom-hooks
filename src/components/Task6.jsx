import React, { useState } from 'react';
import useArrayMethods from '../hooks/useArrayMethods';

const Task6 = () => {
  const [arrElement, setArrElement] = useState('');

  const { push, pop, reverse, reset, resultArr, originalArr } =
    useArrayMethods();

  return (
    <div>
      <h1>6. Array methods</h1>
      <hr />
      <p>Original Array: {originalArr.toString()}</p>
      <label htmlFor='key' className='form-label'>
        Enter a number:
      </label>
      <input
        type='number'
        className='form-control'
        value={arrElement}
        onChange={(e) => setArrElement(e.target.value)}
        onFocus={() => setArrElement('')}
      />
      <button className='btn btn-primary' onClick={() => push(arrElement)}>
        Push
      </button>
      <button className='btn btn-primary ms-2' onClick={pop}>
        Pop
      </button>
      <button className='btn btn-primary ms-2' onClick={reverse}>
        Reverse
      </button>
      <button className='btn btn-primary ms-2' onClick={reset}>
        Reset
      </button>
      <p> Result: {resultArr.toString()}</p>
    </div>
  );
};

export default Task6;
