import React from 'react';
import useCopyClipboard from '../hooks/useCopyClipboard';

const Task7 = () => {
  const { copy } = useCopyClipboard();

  const copyText = () => {
    let content = document.getElementById('copyPara').innerHTML;
    copy(content);
  };
  return (
    <div>
      <h1>7. Copy Clipboard</h1>
      <hr />
      <p id='copyPara'>
        For more straightforward sizing in CSS, we switch the global box-sizing
        value from content-box to border-box. This ensures padding does not
        affect the final computed width of an element, but it can cause problems
        with some third-party software like Google Maps and Google Custom Search
        Engine. On the rare occasion you need to override it, use something like
        the following:
      </p>
      <button className='btn btn-primary' onClick={copyText}>
        Copy Text here
      </button>

      <textarea
        className='form-control'
        placeholder='Paste here'
        id='copiedText'
      ></textarea>
    </div>
  );
};

export default Task7;
