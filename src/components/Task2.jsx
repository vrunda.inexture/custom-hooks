import React from 'react';
import { useWindowSize } from '../hooks/useWindowSize';

const Task2 = () => {
  const { width, height, resolution } = useWindowSize();

  return (
    <div>
      <h1>2. Get Window</h1>
      <hr />
      <p>
        Window : {width}px /{height}px
      </p>
      <p> Resolution: {resolution} </p>
      <p> Window device: {width < 576 ? 'Mobile device' : 'Laptop device'}</p>
    </div>
  );
};

export default Task2;
