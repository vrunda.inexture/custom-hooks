import React, { Fragment, useState } from 'react';
import useStorage from '../hooks/useStorage';

const Task5 = () => {
  const [storageKey, setStorageKey] = useState('');
  const [storageValue, setStorageValue] = useState('');
  const [storageType, setStorageType] = useState('');
  const { saveToLocal, saveToSession } = useStorage();

  // Selecting Storage Type
  const handleClick = () => {
    if (storageType === 'local') {
      saveToLocal(storageKey, storageValue);
    } else if (storageType === 'session') {
      saveToSession(storageKey, storageValue);
    } else {
      window.alert('Please select a storage type!');
    }
  };

  return (
    <Fragment>
      <h1>5. Storages</h1>
      <hr />
      <label htmlFor='key' className='form-label'>
        Key:
      </label>
      <input
        type='text'
        value={storageKey}
        className='form-control'
        id='key'
        onChange={(e) => setStorageKey(e.target.value)}
        onFocus={() => setStorageKey('')}
      />
      <label htmlFor='value' className='form-label mt-3'>
        Value:
      </label>
      <input
        type='text'
        value={storageValue}
        onChange={(e) => setStorageValue(e.target.value)}
        onFocus={() => setStorageValue('')}
        className='form-control'
        id='value'
      />

      <div className='form-check form-check-inline mt-3'>
        <input
          type='radio'
          name='storage'
          id='local'
          value='local'
          className='form-check-input'
          onChange={(e) => setStorageType(e.target.value)}
        />
        <label htmlFor='local' className='form-check-label'>
          Local
        </label>
      </div>
      <div className='form-check form-check-inline'>
        <input
          type='radio'
          name='storage'
          value='session'
          id='session'
          className='form-check-input ms-3'
          onChange={(e) => setStorageType(e.target.value)}
        />
        <label htmlFor='session' className='form-check-label'>
          Session
        </label>
      </div>
      <br />
      <button type='submit' className='btn btn-primary' onClick={handleClick}>
        Save
      </button>
    </Fragment>
  );
};

export default Task5;
